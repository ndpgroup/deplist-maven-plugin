package com.ndpgroup;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.factory.ArtifactFactory;
import org.apache.maven.artifact.manager.WagonManager;
import org.apache.maven.artifact.repository.ArtifactRepository;
import org.apache.maven.artifact.repository.metadata.RepositoryMetadataManager;
import org.apache.maven.artifact.resolver.ArtifactResolver;
import org.apache.maven.artifact.resolver.filter.ArtifactFilter;
import org.apache.maven.model.License;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.MavenProjectBuilder;
import org.apache.maven.project.ProjectBuildingException;
import org.apache.maven.report.projectinfo.ProjectInfoReportUtils;
import org.apache.maven.report.projectinfo.dependencies.Dependencies;
import org.apache.maven.report.projectinfo.dependencies.RepositoryUtils;
import org.apache.maven.settings.Settings;
import org.apache.maven.shared.artifact.filter.ScopeArtifactFilter;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilder;
import org.apache.maven.shared.dependency.graph.DependencyGraphBuilderException;
import org.apache.maven.shared.dependency.graph.DependencyNode;
import org.apache.maven.shared.jar.classes.JarClassesAnalysis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

/**
 * Generate dependency matrix.
 */
@Mojo(name = "csv", requiresDependencyResolution = ResolutionScope.TEST, requiresReports = true, threadSafe = true)
public class DepListMojo extends AbstractMojo {
    /**
     * The maven project.
     */
    @Parameter( defaultValue = "${project}", readonly = true, required = true )
    protected MavenProject project;

    @Parameter( defaultValue = "${project.build.directory}/deplist.csv" )
    protected File outputFile;

    @Parameter( property = "localRepository", required = true, readonly = true )
    protected ArtifactRepository localRepository;

    @Parameter( property = "project.remoteArtifactRepositories" )
    protected List<ArtifactRepository> remoteRepositories;

    @Parameter( defaultValue = "${settings}", required = true, readonly = true )
    protected Settings settings;

    @Component
    protected ArtifactResolver resolver;

    @Component( hint = "default" )
    protected DependencyGraphBuilder dependencyGraphBuilder;

    @Component
    protected JarClassesAnalysis classesAnalyzer;

    @Component
    protected ArtifactFactory artifactFactory;

    @Component
    protected MavenProjectBuilder mavenProjectBuilder;

    @Component
    protected WagonManager wagonManager;

    @Component
    protected RepositoryMetadataManager repositoryMetadataManager;

    public void execute() throws MojoExecutionException
    {
        RepositoryUtils repoUtils = new RepositoryUtils(getLog(), wagonManager, settings, mavenProjectBuilder,
                artifactFactory, resolver, remoteRepositories, project.getPluginRepositories(), localRepository,
                repositoryMetadataManager);

        Dependencies dependencies;
        try {
            ArtifactFilter artifactFilter = new ScopeArtifactFilter(Artifact.SCOPE_TEST);
            DependencyNode dependencyNode = dependencyGraphBuilder.buildDependencyGraph(project, artifactFilter);
            dependencies = new Dependencies(project, dependencyNode, classesAnalyzer);
        } catch (DependencyGraphBuilderException e) {
            throw new MojoExecutionException("Failed to build deplist", e);
        }

        File outputDir = outputFile.getParentFile();
        try {
            if (null != outputDir && !outputDir.exists()) {
                outputDir.mkdirs();
            }
        } catch (Exception e) {
            getLog().warn("Unable to create output dir: " + outputDir, e);
        }

        try {
            try (FileWriter outWriter = new FileWriter(outputFile)) {
                CSVPrinter csvPrinter = new CSVPrinter(outWriter, CSVFormat.DEFAULT);

                Map<String, List<Artifact>> dependenciesByScope = dependencies.getDependenciesByScope(true);
                for (Map.Entry<String, List<Artifact>> scopedDependencyEntry : dependenciesByScope.entrySet()) {
                    for (Artifact artifact : scopedDependencyEntry.getValue()) {
                        addArtifact(csvPrinter, artifact, repoUtils);
                    }
                }
            }
        } catch (IOException e) {
            throw new MojoExecutionException("Failed to write deplist to: " + outputFile.toString(), e);
        }
    }

    void addArtifact(CSVPrinter csvPrinter, Artifact artifact, RepositoryUtils repoUtils) throws IOException {
        String isOptional = artifact.isOptional() ? "optional" : "required";

        String name = artifact.getArtifactId();
        String vendor = artifact.getGroupId();
        String url = ProjectInfoReportUtils.getArtifactUrl(artifactFactory, artifact, mavenProjectBuilder,
                remoteRepositories, localRepository);
        String artifactId = artifact.getArtifactId();
        String licenses = "";
        String description = name;

        MavenProject artifactProject = null;
        try
        {
            artifactProject = repoUtils.getMavenProjectFromRepository( artifact );
        }
        catch ( ProjectBuildingException e )
        {
            getLog().warn("Unable to create Maven project from repository.", e);
        }
        if (null != artifactProject) {
            name = artifactProject.getName();
            if (null != artifactProject.getDescription() && !artifactProject.getDescription().isEmpty()) {
                description = artifactProject.getDescription().replaceAll("\\s+", " ");
            }
            StringJoiner licenseSb = new StringJoiner("; ");
            for (License license : (List<License>)artifactProject.getLicenses()) {
                licenseSb.add(license.getName());
            }
            licenses = licenseSb.toString();
            if (null != artifactProject.getOrganization()) {
                vendor = artifactProject.getOrganization().getName();
            }
        }

        csvPrinter.printRecord(new String[] {
                name, artifact.getVersion(), description, url, vendor, licenses, artifact.getGroupId(), artifactId,
                artifact.getType(), artifact.getScope(), isOptional
            });
    }
}
